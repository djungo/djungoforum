from django.conf import settings

def get_posts_per_page():
	"""
		Retourne le nombre de posts
		à afficher par page.

		Basé sur le paramètre FORUM_POSTS_PER_PAGE
		définit dans le settings.py
	"""
	if hasattr(settings, 'FORUM_POSTS_PER_PAGE'):
		return settings.FORUM_POSTS_PER_PAGE
	return 10





def get_topics_per_page():
	"""
		Retourne le nombre de topics
		à afficher par page.

		Basé sur le paramètre FORUM_TOPICS_PER_PAGE
		définit dans le settings.py
	"""
	if hasattr(settings, 'FORUM_TOPICS_PER_PAGE'):
		return settings.FORUM_TOPICS_PER_PAGE
	return 10