from django import forms

from django.utils.text import slugify
from djungoforum.models import Topic

class NouveauTopicForm(forms.ModelForm):
    class Meta:
        model = Topic
        fields = [
            'auteur',
            'forum',
            'titre',
            'contenu',
        ]


    def __init__ (self, *args, **kwargs):
        """
            Paramètrage fin du formulaire
        """
        # self.producteur = kwargs['initial']['producteur']

        super(NouveauTopicForm, self).__init__(*args, **kwargs)
        # Définition des classes des widgets
        self.fields['auteur'].widget = forms.HiddenInput()
        self.fields['forum'].widget = forms.HiddenInput()
        self.fields['titre'].widget.attrs['class'] = 'form-control'
        self.fields['titre'].widget.attrs['placeholder'] = 'Sujet...'
        self.fields['contenu'].widget.attrs['class'] = 'form-control'

    def clean_titre(self):
        """
            La permission du topic n'est permise que si un Topic
            portant le même titre n'éxiste pas déjà dans le Forum
        """
        titre = self.cleaned_data['titre']
        slug_titre = slugify(titre)
        forum = self.cleaned_data['forum']
        
        if Topic.objects.filter(slug=slug_titre, forum=forum).exists():
            raise forms.ValidationError('Un Topic portant ce titre éxiste déjà dans ce Forum')

        return titre