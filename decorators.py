from django.conf import settings
from django.shortcuts import HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy

from djungoforum.models import Forum

def forum_acccessible(view_func):
    """
        Permet l'écécution de la méthode que si le forum est publique ou
        si l'utilisateur est authentifié et dispose des permission necessaires
    """
    def _wrapped_view_func(request, *args, **kwargs):
        forum = Forum.objects.get(slug=kwargs['slug_forum'], categorie__slug=kwargs['slug_categorie'])

        # Définition de l'utilisateur du Forum
        if request.user.is_authenticated():
            forum_membre = settings.GET_FORUM_USER_FROM_ADHERENT(request.user)
        else:
            forum_membre = None
        
        # Le Forum est-il accessible (publique ou persmissions requises)
        accessible = forum.est_accessible(forum_membre)

        # Execution ou redirection
        if not accessible:
            return HttpResponseRedirect(reverse_lazy('home_forum'))
        else:
            return view_func(request, *args, **kwargs)
            
    return _wrapped_view_func