# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gestion_compte', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Categorie',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('titre', models.CharField(max_length=255)),
                ('slug', models.SlugField(max_length=255)),
                ('description', models.TextField(blank=True, null=True)),
                ('date_creation', models.DateTimeField(auto_now_add=True)),
                ('date_modification', models.DateTimeField(auto_now=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Forum',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('titre', models.CharField(max_length=255)),
                ('slug', models.SlugField(max_length=255)),
                ('description', models.TextField(blank=True, null=True)),
                ('date_creation', models.DateTimeField(auto_now_add=True)),
                ('date_modification', models.DateTimeField(auto_now=True, null=True)),
                ('categorie', models.ForeignKey(to='djungoforum.Categorie')),
            ],
        ),
        migrations.CreateModel(
            name='ForumGroupe',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('nom', models.CharField(max_length=50)),
                ('description', models.TextField(blank=True, null=True)),
                ('forum_membres', models.ManyToManyField(to='gestion_compte.Adherent')),
            ],
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('contenu', models.TextField()),
                ('date_creation', models.DateTimeField(auto_now_add=True)),
                ('date_modification', models.DateTimeField(auto_now=True, null=True)),
                ('auteur', models.ForeignKey(to='gestion_compte.Adherent')),
            ],
        ),
        migrations.CreateModel(
            name='Topic',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('titre', models.CharField(max_length=255)),
                ('slug', models.SlugField(max_length=255)),
                ('contenu', models.TextField()),
                ('sticky', models.BooleanField(default=False)),
                ('vues', models.BigIntegerField(default=0)),
                ('date_dernier_post', models.DateTimeField(auto_now_add=True)),
                ('date_creation', models.DateTimeField(auto_now_add=True)),
                ('date_modification', models.DateTimeField(auto_now=True, null=True)),
                ('auteur', models.ForeignKey(to='gestion_compte.Adherent')),
                ('forum', models.ForeignKey(to='djungoforum.Forum')),
            ],
            options={
                'ordering': ['-sticky', '-date_dernier_post'],
            },
        ),
        migrations.CreateModel(
            name='TopicRead',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('date_lecture', models.DateTimeField()),
                ('forum_membre', models.ForeignKey(to='gestion_compte.Adherent')),
                ('topic', models.ForeignKey(to='djungoforum.Topic')),
            ],
        ),
        migrations.AddField(
            model_name='post',
            name='topic',
            field=models.ForeignKey(to='djungoforum.Topic'),
        ),
        migrations.AddField(
            model_name='forum',
            name='forum_groupes_acces',
            field=models.ManyToManyField(blank=True, to='djungoforum.ForumGroupe'),
        ),
    ]
