from django import template
from django.conf import settings

from djungoforum.models import Post

register = template.Library()


@register.filter
def topic_lu(topic, user):
	"""
		Retourne Vrai si le Topic a été entièrement lu
		par l'utilisateur du Forum
	"""
	if user.is_authenticated():
		forum_membre = settings.GET_FORUM_USER_FROM_ADHERENT(user)
		return topic.lu(forum_membre)


@register.filter
def topic_non_lu_count(topic, user):
	"""
		Retourne le nombre de Posts non lus par
		l'utilisateur du Forum dans le Topic
	"""
	if user.is_authenticated():
		forum_membre = settings.GET_FORUM_USER_FROM_ADHERENT(user)
		return topic.posts_non_lus_count(forum_membre)


@register.filter
def topic_date_lecture(topic, user):
	"""
		Retourne la date de lecture du Topic par
		l'utilisateur du forum
	"""
	if user.is_authenticated():
		forum_membre = settings.GET_FORUM_USER_FROM_ADHERENT(user)
		return forum_membre.date_lecture_topic(topic)


@register.filter
def forum_lu(forum, user):
	"""
		Retourne Faux si le Forum contient des Topics
		non lus par l'utilisateur du Forum
	"""
	if user.is_authenticated():
		forum_membre = settings.GET_FORUM_USER_FROM_ADHERENT(user)
		return forum.lu(forum_membre)


@register.filter
def forums_accessibles(categorie, user):
	"""
		Retourne la liste des forums accessibes
		dans la Catégorie pour l'Utilisateur du Forum.
		Ainsi que les Forums sans restriction d'accès.
	"""
	if user.is_authenticated():
		forum_membre = settings.GET_FORUM_USER_FROM_ADHERENT(user)
	else:
		forum_membre = None
	
	return categorie.forums_accessibles(forum_membre)