from django.db import models
from django.db.models import Q

from django.dispatch import receiver
from django.db.models.signals import pre_delete
from django.contrib.auth.models import Group as DjangoGroup

from django.conf import settings
from django.apps import apps
get_model = apps.get_model

from django.core.urlresolvers import reverse_lazy
from django.utils.text import slugify

from datetime import datetime

MEMBRE = settings.FORUM_USER

def get_class_membre():
    return get_model(*MEMBRE.split('.',1))


class ForumMembre(models.Model):
    """
        Model Abstrait dont doit hériter le Model Utilisateur du projet.
    """

    forum_groupes = models.ManyToManyField('ForumGroupe')

    class Meta:
        abstract = True


    @property
    def posts(self):
        """
            Retourne la liste des posts de l'utilisateur
        """
        return self.post_set.all()


    @property
    def posts_count(self):
        """
            Retourne le nombre de post réalisé par l'utilisateur
        """
        return self.posts.count()


    @property
    def topics(self):
        """
            Retourne la liste des topics de l'utilisateur
        """
        return self.topic_set.all()


    @property
    def topics_count(self):
        """
            Retourne le nombre de topics réalisés par l'utilisateur
        """
        return self.topics.count()


    @property
    def messages_count(self):
        """
            Retourne le nombre de post et de topics créés par l'utilisateur
        """
        return self.posts_count + self.topics_count


    @property
    def forum_avatar(self):
        """
            Retourne l'avatar de l'utilisateur associé
            s'il dispose d'un attribut 'avatar'
        """
        if hasattr(self, 'avatar') and self.avatar:
            return self.avatar.url
        else:
            return "http://placehold.it/64x64&text=No Image"


    def lire_topic(self, topic):
        """
            Renseigne que l'utilisateur a lu le Topic passé en paramètre
        """
        # Création/Récupération du TopicRead correspondant
        defaults_topic_read = {'date_lecture': datetime.now()}
        topic_read, topic_read_cree = TopicRead.objects.get_or_create(
            forum_membre=self,
            topic=topic,
            defaults=defaults_topic_read
        )
        
        # MAJ de la date de lecture si le Topic a déjà été lu avant
        if not topic_read_cree:
            topic_read.date_lecture = datetime.now()
            topic_read.save()


    def date_lecture_topic(self, topic):
        """
            Retourne la date a laquelle l'utilisteur du forum
            a lu le Topic
        """
        return TopicRead.objects.get(forum_membre=self, topic=topic)

    @property
    def forum_groupes(self):
        """
            Retourne les Groupes du Forum auquels appartient
            l'utilisateur du Forum.
        """
        return self.forumgroupe_set.all()






class ForumGroupe(models.Model):
    """
        Groupe dédié au forum, permet la restriction ou permission d'accès à certains Forums
    """
    nom = models.CharField(max_length=50)
    description = models.TextField(null=True, blank=True)

    forum_membres = models.ManyToManyField(MEMBRE)


    def save(self, *args, **kwargs):
        """
            Créé un DjangoGroup lors de la création
        """
        CREATE = not self.pk

        if CREATE:
            DjangoGroup.objects.create(name=self.nom)
        else:
            ancien_groupe = ForumGroupe.objects.get(pk=self.pk)
            django_groupe = DjangoGroup.objects.get(name=ancien_groupe.nom)
            django_groupe.name = self.nom
            django_groupe.save()
        
        super(ForumGroupe, self).save(*args, **kwargs)


    def __str__(self):
        return self.nom


@receiver(pre_delete, sender=ForumGroupe)
def pre_delete(sender, instance, **kwargs):
    """
        Supprime le DjangoGroup associé lors de la création
    """
    try:
        django_groupe = DjangoGroup.objects.get(name=instance.nom)
        django_groupe.delete()
    except Exception as e:
        # Le groupe a déjà été supprimé, il n'éxiste déjà plus.
        pass
    
        





class Categorie(models.Model):
    titre = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255)
    description = models.TextField(null=True, blank=True)

    date_creation = models.DateTimeField(auto_now_add=True)
    date_modification = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return self.titre

    @property
    def forums(self):
        """
            Retourne la liste des forums de la catégorie
        """
        return self.forum_set.all()

    @property
    def forums_count(self):
        """
            Retourne le nombre de forums de la catégorie
        """
        return self.forums.count()


    def forums_accessibles(self, forum_membre=None):
        """
            Retourne la liste des forums accessibles par
            l'Utilisateur du Forum passé en paramètre.
            Ainsi que les Forums sans restriction d'accès.
        """
        if forum_membre:
            groupes_utilisateur = forum_membre.forum_groupes
            forums = self.forums.filter(Q(forum_groupes_acces__in=groupes_utilisateur) | Q(forum_groupes_acces=None))
        else:
            forums = self.forums.filter(forum_groupes_acces=None)
        return forums.distinct()





class Forum(models.Model):
    titre = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255)
    description = models.TextField(null=True, blank=True)

    categorie = models.ForeignKey(Categorie)

    forum_groupes_acces = models.ManyToManyField(ForumGroupe, blank=True)

    date_creation = models.DateTimeField(auto_now_add=True)
    date_modification = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return self.titre

    @property
    def topics(self):
        """
            Retourne la liste des topics du forum
        """
        return self.topic_set.all()

    @property
    def topics_non_vide(self):
        """
            Retourne la liste des topics du forum contenant
            au moins un Post
        """
        return [topic for topic in self.topics if topic.posts.exists()]

    @property
    def topics_count(self):
        """
            Retourne le nombre de topics du forum
        """
        return self.topics.count()

    @property
    def posts(self):
        """
            Retourne les posts des topics du forum
        """
        return Post.objects.filter(topic__in=self.topics)

    @property
    def posts_count(self):
        """
            Retourne le nombre de posts dans chacun
            des topics du forum
        """
        return self.posts.count()

    @property
    def last_post(self):
        """
            Retourne le dernier Post publié dans le forum
        """
        return self.posts.last()


    def topics_non_lus(self, forum_membre, *args, **kwargs):
        """
            Retourne les Topics nons lus par l'utilisateur
            dans le Forum conçerné.
            Ne retourne pas un Queryset.
        """
        return [topic for topic in self.topics if not topic.lu(forum_membre=forum_membre)]

    def lu(self, forum_membre):
        """
            Retourne Vrai si le Forum comporte des Topics non lus
            par l'utilisateur du forum
        """
        return not self.topics_non_lus(forum_membre)

    @property
    def groupes_acces(self):
        """
            Retourne la liste des groupes ayant accès à ce Forum
        """
        return self.forum_groupes_acces.all()

    @property
    def prive(self):
        """
            Retourne Vrai si le Forum est privé
        """
        return self.groupes_acces.exists()

    @property
    def publique(self):
        """
            Retourne Vrai si le Forum est publique
        """
        return not self.prive

    def est_accessible(self, forum_membre=None):
        """
            Retourne Vrai si le forum est accessibles par
            l'Utilisateur du Forum passé en paramètre ou si 
            le Forum est publique.
        """
        if forum_membre:
            groupes_utilisateur = forum_membre.forum_groupes
            accessible = self.publique or self.groupes_acces.filter(pk__in=groupes_utilisateur.values('pk')).exists()
        else:
            accessible = self.publique

        return accessible








class Topic(models.Model):
    titre = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255)
    contenu = models.TextField()
    sticky = models.BooleanField(default=False)
    vues = models.BigIntegerField(default=0)
    date_dernier_post = models.DateTimeField(auto_now_add=True)

    forum = models.ForeignKey(Forum)

    date_creation = models.DateTimeField(auto_now_add=True)
    date_modification = models.DateTimeField(auto_now=True, null=True, blank=True)

    auteur = models.ForeignKey(MEMBRE)

    class Meta:
        ordering = ['-sticky', '-date_dernier_post']

    def __str__(self):
        return self.titre

    @property
    def posts(self):
        """
            Retourne la liste des posts du topic
        """
        return self.post_set.all()

    @property
    def last_post(self):
        """
            Retourne le dernier post pulié sur le topic
        """
        return self.posts.last()

    @property
    def posts_count(self):
        """
            Retourne le nombre de posts du topic
        """
        return self.posts.count()


    def get_absolute_url(self):
        return reverse_lazy('afficher_topic',
            kwargs={
                'slug_categorie': self.forum.categorie.slug,
                'slug_forum': self.forum.slug,
                'slug_topic': self.slug,
            }
        )


    def save(self, *args, **kwargs):
        """
            Génération automatique du slug du Topic
        """
        self.slug = slugify(self.titre)
        super(Topic, self).save(*args, **kwargs)

    
    def posts_non_lus(self, forum_membre):
        """
            Retourne les posts nons lus par l'utilisateur
            dans le Topic
        """
        derniere_lecture = TopicRead.objects.filter(topic=self, forum_membre=forum_membre)
        deja_lu = derniere_lecture.exists()

        if deja_lu:
            posts_non_lus = self.posts.filter(date_creation__gte=derniere_lecture.first().date_lecture)
        else:
            posts_non_lus = self.posts

        return posts_non_lus
        

    def posts_non_lus_count(self, forum_membre):
        """
            Retourne le nombre de posts nons lus par l'utilisateur
            dans le Topic
        """
        return self.posts_non_lus(forum_membre).count()


    @property
    def nouveau(self):
        """
            Retourne Vrai si le Topic ne comporte aucun Post,
            s'il est nouveau.
        """
        return not self.posts.exists()


    def lu(self, forum_membre):
        """
            retourne Vrai si le Topic ne contient aucun
            Post non lu par l'utilisateur du Forum dont
            il n'est pas l'auteur
        """
        # Définition de si le Topic a déjà été lu par l'utilisateur
        derniere_lecture = TopicRead.objects.filter(topic=self, forum_membre=forum_membre)
        deja_lu = derniere_lecture.exists()

        # Si le Topic a déjà été lu, définition de l'éxistence de nouveaux Posts dans le Topic
        # depuis la dernière lecture du Topic par l'utilisateur
        # en escluant les topics dont l'utilisateur est l'auteur
        nouveaux_posts = False
        if deja_lu:
            nouveaux_posts = self.posts.filter(date_creation__gte=derniere_lecture.first().date_lecture)
            nouveaux_posts = nouveaux_posts.exclude(pk__in=Post.objects.filter(auteur=forum_membre).values('pk'))
            nouveaux_posts = nouveaux_posts.exists()

        return deja_lu and not nouveaux_posts







class Post(models.Model):
    contenu = models.TextField()

    topic = models.ForeignKey(Topic)

    date_creation = models.DateTimeField(auto_now_add=True)
    date_modification = models.DateTimeField(auto_now=True, null=True, blank=True)

    auteur = models.ForeignKey(MEMBRE)

    def __str__(self):
        return "{} - {}".format(self.pk, self.contenu[:40])

    def lu(self, forum_membre):
        """
            Retourne Vrai si le Post a été lu par l'utilisateur du forum
        """
        return self in forum_membre.posts_lus


    def get_date_lecture(self, forum_membre):
        """
            Retourne la date à laquelle le Post a été lu
        """
        return PostRead.objects.get(post=self, forum_membre=forum_membre)


    def save(self, *args, **kwargs):
        """
            Modifie la date du dernier Post dans le Topic parent
        """
        self.topic.date_dernier_post = datetime.now()
        self.topic.save()
        super(Post, self).save(*args, **kwargs)





class TopicRead(models.Model):
    """
        Enregistre la date à laquelle un Utilisateur du Forum
        a lu le contenu initial du Topic
    """
    date_lecture = models.DateTimeField()

    forum_membre = models.ForeignKey(MEMBRE)
    topic = models.ForeignKey(Topic)

    def __str__(self):
        return str(self.date_lecture)