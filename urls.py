from django.conf.urls import url
from djungoforum.views import Home, ListeCategories, AfficherCategorie, AfficherForum, AfficherTopic, Poster,\
                        NouveauTopic

from django.conf import settings
from django.conf.urls.static import static

from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^topic/(?P<pk_topic>\d+)/poster/$', 
        Poster.as_view(), 
        name='poster'
    ),

    url(r'^$',
    	ListeCategories.as_view(),
    	name='home_forum'
    ),
    
    url(r'^categories/$',
    	ListeCategories.as_view(),
    	name='liste_categories'
    ),

	url(r'^(?P<slug>[\w-]+)/$',
    	AfficherCategorie.as_view(),
    	name='afficher_categorie'
    ),
    
    url(r'^(?P<slug_categorie>[\w-]+)/(?P<slug_forum>[\w-]+)/$',
    	AfficherForum.as_view(),
    	name='afficher_forum'
    ),

    url(r'^(?P<slug_categorie>[\w-]+)/(?P<slug_forum>[\w-]+)/nouveau-topic/$',
        login_required(NouveauTopic.as_view()),
        name='nouveau_topic'
    ),

    url(r'^(?P<slug_categorie>[\w-]+)/(?P<slug_forum>[\w-]+)/(?P<slug_topic>[\w-]+)/$',
    	AfficherTopic.as_view(),
    	name='afficher_topic'
    ),


] 