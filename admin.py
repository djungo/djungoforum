from django.contrib import admin
from djungoforum.models import Categorie, Forum, Topic, Post, ForumGroupe

class CategorieAdmin(admin.ModelAdmin):
	prepopulated_fields = {"slug": ("titre",)}
admin.site.register(Categorie, CategorieAdmin)

class ForumAdmin(admin.ModelAdmin):
	prepopulated_fields = {"slug": ("titre",)}
admin.site.register(Forum, ForumAdmin)

class TopicAdmin(admin.ModelAdmin):
	prepopulated_fields = {"slug": ("titre",)}
admin.site.register(Topic, TopicAdmin)

admin.site.register(Post)
admin.site.register(ForumGroupe)