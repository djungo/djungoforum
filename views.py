from django.shortcuts import HttpResponse
from django.http import HttpResponseForbidden
from django.views.generic import TemplateView, ListView, DetailView, View, CreateView

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from djungoforum.models import Categorie, Forum, Topic, Post, get_class_membre
from djungoforum.forms import NouveauTopicForm

from django.conf import settings
UserForum = get_class_membre() 

from django.http import JsonResponse
from django.template import Context
from django.template.loader import get_template

from django.utils.decorators import method_decorator
from djungoforum.decorators import forum_acccessible

from djungoforum.utils import get_posts_per_page, get_topics_per_page


class Home(TemplateView):
    """
        Page d'accueil du forum.
        Liste les catégories et
        les forums qui les composent
    """
    template_name = 'djungoforum/home.html'





class ListeCategories(ListView):
    """
        Affiche la liste des catégories
    """
    model = Categorie
    context_object_name = 'categories'
    template_name = 'djungoforum/categorie/liste_categories.html'





class AfficherCategorie(DetailView):
    """
        Affiche la catégorie
    """
    model = Categorie
    context_object_name = 'categorie'
    template_name = 'djungoforum/categorie/afficher_categorie.html'







class AfficherForum(DetailView):
    """
        Affiche un forum
    """
    model = Forum
    context_object_name = 'forum'
    template_name = 'djungoforum/forum/afficher_forum.html'

    @method_decorator(forum_acccessible)
    def dispatch(self, *args, **kwargs):
        return super(AfficherForum, self).dispatch(*args, **kwargs)

    def get_object(self, *args, **kwargs):
        """
            Retourne le model Forum, correspondant
            au slug du forum et de la catégorie
        """
        slug_forum = self.kwargs['slug_forum']
        slug_categorie = self.kwargs['slug_categorie']

        return Forum.objects.get(slug=slug_forum, categorie__slug=slug_categorie)

    def get_context_data(self, *args, **kwargs):
        """
            Ajoute un paginator au contexte
            basé sur le nombre de topics à afficher par
            page définit dans le settings.py
        """
        context = super(AfficherForum, self).get_context_data(*args, **kwargs)

        paginator = Paginator(
            self.object.topics,
            get_topics_per_page()
        )
        page = self.request.GET.get('page')
        try:
            topics = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            topics = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            topics = paginator.page(paginator.num_pages)
        context['topics'] = topics

        return context





class NouveauTopic(CreateView):
    """
        Créé un nouveau topic pour la catégorie spécifiée
    """
    form_class = NouveauTopicForm
    template_name = 'djungoforum/topic/nouveau_topic.html'

    def get_initial(self, *args, **kwargs):
        initial = super(NouveauTopic, self).get_initial(*args, **kwargs)
        initial['auteur'] = settings.GET_FORUM_USER_FROM_ADHERENT(self.request.user)
        initial['forum'] = Forum.objects.get(
            categorie__slug=self.kwargs['slug_categorie'],
            slug=self.kwargs['slug_forum']
        )
        return initial

    def get_context_data(self, *args, **kwargs):
        """
            Ajoute au contexte, le Forum dans lequel sera ajouté le Topic
        """
        context = super(NouveauTopic, self).get_context_data(**kwargs)
        context["forum"] = Forum.objects.get(
            categorie__slug=self.kwargs['slug_categorie'],
            slug=self.kwargs['slug_forum']
        )
        return context






class AfficherTopic(DetailView):
    """
        Affiche un topic
    """
    model = Topic
    context_object_name = 'topic'
    template_name = 'djungoforum/topic/afficher_topic.html'

    def get_object(self, *args, **kwargs):
        """
            Retourne le model Forum, correspondant
            au slug du forum et de la catégorie
        """
        slug_topic = self.kwargs['slug_topic']
        slug_forum = self.kwargs['slug_forum']
        slug_categorie = self.kwargs['slug_categorie']
        
        return Topic.objects.get(
            slug=slug_topic,
            forum__slug=slug_forum,
            forum__categorie__slug=slug_categorie
        )

    def get(self, *args, **kwargs):
        """
            Incrémente le nombre de fois où le topic a été visualisé
            
            Si l'internaute est authentifié alors on enregistre 
            la date à laquelle il a lu les Posts du Topic.
        """
        topic = self.get_object()

        # Incrément du nombre de vues
        topic.vues += 1
        topic.save()

        # Enregistrement de la date de lecture du Topic
        if self.request.user.is_authenticated():
            forum_membre = settings.GET_FORUM_USER_FROM_ADHERENT(self.request.user)
            topic = self.get_object()
            forum_membre.lire_topic(topic)

        return super(AfficherTopic, self).get(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        """
            Ajoute un paginator au contexte
            basé sur le nombre de posts à afficher par
            page définit dans le settings.py
        """
        context = super(AfficherTopic, self).get_context_data(*args, **kwargs)

        paginator = Paginator(
            self.object.posts,
            get_posts_per_page()
        )
        page = self.request.GET.get('page')
        try:
            posts = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            posts = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            posts = paginator.page(paginator.num_pages)
        context['posts'] = posts

        return context





class Poster(View):
    def post(self, request, pk_topic):
        """
            Enregistre le Post de l'utilisateur.
            L'utilisateur doit être authentifié.

            Retourne le code HTMl permettant l'affichage
            du Post en se basant sur une brick.
        """
        # Contrôle de l'authentification de l'utilisateur
        if not self.request.user.is_authenticated():
            return HttpResponseForbidden('Vous devez être authentifié afin de pouvoir poster.')
        # Contrôle du contenu non vide
        if not self.request.POST['contenu']:
            return HttpResponseForbidden('Vous devez saisir un message.')

        # Récupération des données du Post
        auteur = settings.GET_FORUM_USER_FROM_ADHERENT(self.request.user)
        topic = Topic.objects.get(pk=pk_topic)
        contenu = self.request.POST['contenu']
        # Création du Post
        post = Post.objects.create(
            auteur=auteur,
            topic=topic,
            contenu=contenu
        )
        # Enregistrement de la lecture du Post
        auteur.lire_topic(topic)
        # Création de la réponse compose du Post créé
        c = Context(dict(post=post, user=self.request.user))
        post_html = get_template('djungoforum/bricks/post.html').render(c)

        json_data_response = {
            'post_html': post_html
        }
        response = JsonResponse(json_data_response)

        return response